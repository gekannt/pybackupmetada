### PyBackupmetadata


This program helps you to back up all important metadata that are necessary to fast restoring of the system or for making one more computer with the same configuration as current one. 



**Please note that no user's data are saved besides settings.**

The most important settings reside usually in /etc directory, 
all directories/files located at $HOME path. Also the list of all ever installed packages is backed up to file so you can run ll commands from it on new machine again.


To launch program do: 
 python main.py
 
Upon successful completion you will get the following files:
   install.txt 
   $HOME.zip
   etc.zip 

