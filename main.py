
__author__ = 'A. Shulzhenko'

import commands, re

INSTALL_FILE = 'install.txt'

def checkForError(result,message):
    if result[0]:
        print "error happened " + result[0] +  ' '   + result[1] + ' ' + message
        exit(-1)


def main():

    ## Start of block
    #  Gathers all installed packages into install.txt file so they
    # can be relaunched smoothly on the fresh system

    print "getting installed packages"
    command = "( zcat $( ls -tr /var/log/apt/history.log*.gz ) ; cat /var/log/apt/history.log ) |" \
              " egrep '^(Start-Date:|Commandline:)' | grep -v aptdaemon | egrep '^Commandline:' |" \
              " egrep 'install'"
    lines = re.sub('Commandline:', '', commands.getoutput(command))

    with open(INSTALL_FILE, "w") as fileText:
        fileText.write(lines)
    ##End of block


    ## Start of block
    # /Etc directory is crucial, it contains many important settings - cron, apache,  mysql etc.
    print "backing up etc directory"
    command = "zip etc.zip -r /etc/"
    res = commands.getoutput(command)

    ##End of block


    ## Start of block
    # All files and dirs in  $HOME directory starting with dot symbol shoudl be backed up too

    print "saving settings from home dir"
    # besides .cache directory
    command = "find  $HOME -maxdepth 1  -name  '\.*' | egrep -v '.cache$' | tr '\n' '!'   "
    lines = commands.getoutput(command)

    lines = re.split('!', lines)
    lines = filter(None, lines)

    command = "zip home_" + commands.getoutput('echo $USER') + '.zip -r '

    for line in lines:
        command = command + line + ' '

    print command
    result = commands.getstatusoutput(command)

    ##End of block

    print "back up finished successfully"

if __name__ == "__main__":
    main()
